export const PI = Math.PI;

export let usuario = "Juan";
let password = "qwerty"; 


// let exportarDefault = "Hola mundo!";   // solo puede haber un export default 
// export default exportarDefault;


export default function saludar(nombre)  // solo se puede exportar una function por defecto default
{
    console.log("Hola módulos ECMASript6 +ES6, soy "+nombre);
}

export class Saludar
{
    constructor()
    {
        console.log("Hola módulos soy una clase ECMASript6 +ES6");
    }
}